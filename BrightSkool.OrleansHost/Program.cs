﻿using System;
using Orleans;
using Orleans.Runtime.Configuration;
using System.Threading.Tasks;
using BrightSkool.OrleansImplementations.Masters;

namespace BrightSkool.OrleansHost
{
    /// <summary>
    /// Orleans silo host
    /// </summary>
    public class Program
    {
        static void Main(string[] args)
        {
            // The Orleans silo environment is initialized in its own app domain in order to more
            // closely emulate the distributed situation, when the client and the server cannot
            // pass data via shared memory.
            AppDomain hostDomain = AppDomain.CreateDomain("OrleansHost", null, new AppDomainSetup
            {
                AppDomainInitializer = InitSilo,
                AppDomainInitializerArguments = args,
            });

            var config = ClientConfiguration.LocalhostSilo();
            GrainClient.Initialize(config);
            createMasterGrains();
            Console.WriteLine("Orleans Silo is running.\nPress Enter to terminate...");
            Console.ReadLine();

            hostDomain.DoCallBack(ShutdownSilo);
        }

        static void InitSilo(string[] args)
        {
            hostWrapper = new OrleansHostWrapper(args);

            if (!hostWrapper.Run())
            {
                Console.Error.WriteLine("Failed to initialize Orleans silo");
            }
        }

        static void ShutdownSilo()
        {
            if (hostWrapper != null)
            {
                hostWrapper.Dispose();
                GC.SuppressFinalize(hostWrapper);
            }
        }

        private static OrleansHostWrapper hostWrapper;

        /// <summary>
        /// create grains for all master data asynchronously.
        /// </summary>
        /// <returns></returns>
        private static async void createMasterGrains()
        {
            //CountryList.SetupCountryListGrain().
            //    ContinueWith(_ => CurrencyList.SetupCurrencyListGrain())
            //    .ContinueWith(_ => LanguageList.SetupLanguageListGrain())
            //    .ContinueWith(_ => NationalityList.SetupNationalityListGrain())
            //    .ContinueWith(_ => RaceList.SetupRaceListGrain())
            //    .ContinueWith(_ => RelationshipList.SetupRelationshipListGrain())
            //    .ContinueWith(_ => ReligionList.SetupReligionListGrain())
            //    .ContinueWith(_ => ApplicationStatusList.SetupApplicationStatusListGrain())
            //    .Wait();

            await CountryList.SetupCountryListGrain();
            await CurrencyList.SetupCurrencyListGrain();
            await LanguageList.SetupLanguageListGrain();
            await NationalityList.SetupNationalityListGrain();
            await RaceList.SetupRaceListGrain();
            await RelationshipList.SetupRelationshipListGrain();
            await ReligionList.SetupReligionListGrain();
            await ApplicationStatusList.SetupApplicationStatusListGrain();

        }
    }
}
//F:\Projects\Brightsword\Received\CICD_Testing\BrightSkoolProject\BrightSkool.OrleansHost\BrightSkool.OrleansHost.sln