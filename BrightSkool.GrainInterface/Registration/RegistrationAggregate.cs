﻿using BrightSkool.Domain.Models;
using Patterns.EventSourcing.Interface;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrightSkool.GrainInterface.Registration
{
    public class RegistrationAggregate : ICanApplyEvent<RegistrationOperations, RegistrationAggregate>
    {
        private string _status;
        public RegistrationModel _registrationModel { get; set; }
        public RegistrationAggregate(RegistrationModel registrationmodel)
        {
            _registrationModel = registrationmodel;
        }

        public RegistrationAggregate(string status, RegistrationModel registrationmodel)
        {
            _registrationModel = registrationmodel;
            _status = status;
        }

        public RegistrationAggregate() { }

        public RegistrationAggregate ApplyEvent(TimestampedValue<RegistrationOperations> value, RegistrationAggregate currentState)
        {
            return value.Value.Match(SaveRegistration(currentState), ProcessRegistration(_status, currentState));
        }

        private static Func<RegistrationModel, RegistrationAggregate> SaveRegistration(RegistrationAggregate current)
        {
            return registration => new RegistrationAggregate(current._registrationModel = registration);
        }

        private static Func<string, RegistrationModel, RegistrationAggregate> ProcessRegistration(string status, RegistrationAggregate current)
        {
            return (RegistrationStatus, RegistrationModel) => new RegistrationAggregate(current._status = RegistrationStatus, current._registrationModel = RegistrationModel);
        }
    }
}
