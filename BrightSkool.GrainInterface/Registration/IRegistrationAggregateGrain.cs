﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Patterns.Aggregates.Interface;

namespace BrightSkool.GrainInterface.Registration
{
    public interface IRegistrationAggregateGrain :
        IAggregateGrain<IRegistration, RegistrationOperations, RegistrationState, RegistrationAggregate>
    {
    }
}
