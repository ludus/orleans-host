﻿using BrightSkool.Domain.Models;
using Patterns.EventSourcing.Interface;
using System.Threading.Tasks;

namespace BrightSkool.GrainInterface.Registration
{
    /// <summary>
    /// Grain interface ISchoolProfile
    /// </summary>
    public interface IRegistration : IEventSourcedGrain<RegistrationOperations, RegistrationState>
    {
        Task SaveRegistration(RegistrationModel registrationmodel);
        Task ProcessRegistration(string status);
        Task<RegistrationModel> GetRegistration();
    }
}
