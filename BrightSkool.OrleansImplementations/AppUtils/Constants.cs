﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrightSkool.OrleansImplementations.AppUtils
{
    public class Constants
    {
        public const string CountryList = "CountryList";
        public const string CurrencyList = "CurrencyList";
        public const string RaceList = "RaceList";
        public const string ReligionList = "ReligionList";
        public const string NationalityList = "NationalityList";
        public const string LanguageList = "LanguageList";
        public const string RelationshipList = "RelationshipList";
        public const string ApplicationStatusList = "ApplicationStatusList";
        public const string storageContainer = "tmsv2orleansstore";
        public const string connectionStringKey = "DataConnectionString";
    }
}
