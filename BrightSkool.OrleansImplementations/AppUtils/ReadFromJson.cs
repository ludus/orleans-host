﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Blob;
using Microsoft.WindowsAzure.ServiceRuntime;

namespace BrightSkool.OrleansImplementations.AppUtils
{
    public class ReadFromJson
    {
        public static async Task<List<T>> ReadDataFromJson<T>(string fileName)
        {
            List<T> lstObjectData = new List<T>();
            try
            {
                CloudStorageAccount storageAccount = CloudStorageAccount.Parse(RoleEnvironment.GetConfigurationSettingValue(Constants.connectionStringKey));
                CloudBlobClient blobclient = storageAccount.CreateCloudBlobClient();
                CloudBlobContainer container = blobclient.GetContainerReference(Constants.storageContainer);

                foreach (IListBlobItem item in container.ListBlobs(fileName, false))
                {
                    if (item.GetType() == typeof(CloudBlockBlob))
                    {
                        CloudBlockBlob blob = (CloudBlockBlob)item;

                        var jsondata = await Task.Run(() => blob.DownloadText());
                        if (!jsondata.Equals(string.Empty)) { lstObjectData = JsonConvert.DeserializeObject<List<T>>(jsondata); }
                    }
                }

                //var filePath = string.Concat(ConfigurationManager.AppSettings["FilePath"], fileName, ".json");
                //var jsonData = await Task.Run(() => File.ReadAllText(filePath));
                //if (!jsonData.Equals(string.Empty)) { lstObjectData = JsonConvert.DeserializeObject<List<T>>(jsonData); }
            }
            catch (Exception ex)
            { throw ex; }
            return lstObjectData;
        }
    }
}
