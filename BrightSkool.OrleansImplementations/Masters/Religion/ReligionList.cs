﻿using BrightSkool.GrainInterface.Masters;
using BrightSkool.OrleansImplementations.AppUtils;
using Orleans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.OrleansImplementations.Masters
{
    public class ReligionList
    {
        public static async Task SetupReligionListGrain()
        {
            List<Religion> lstReligions = new List<Religion>();
            try
            {
                lstReligions = await ReadFromJson.ReadDataFromJson<Religion>(Constants.ReligionList);
                var grain = GrainClient.GrainFactory.GetGrain<IReligion>(Constants.ReligionList);
                await grain.SetItem(lstReligions);
            }
            catch (Exception) { }
        }
    }
}
