﻿using BrightSkool.GrainInterface.Masters;
using BrightSkool.OrleansImplementations.AppUtils;
using Orleans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.OrleansImplementations.Masters
{
    public class CountryList
    {
        public static async Task SetupCountryListGrain()
        {
            List<Country> lstCountries = new List<Country>();
            try
            {
                lstCountries = await ReadFromJson.ReadDataFromJson<Country>(Constants.CountryList);
                var grain = GrainClient.GrainFactory.GetGrain<ICountry>(Constants.CountryList);
                await grain.SetItem(lstCountries);
            }
            catch (Exception ex) { throw ex; }
        }
    }
}
