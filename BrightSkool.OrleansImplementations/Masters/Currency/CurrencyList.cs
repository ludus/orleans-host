﻿using BrightSkool.GrainInterface.Masters;
using BrightSkool.OrleansImplementations.AppUtils;
using Orleans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.OrleansImplementations.Masters
{
    public class CurrencyList
    {
        public static async Task SetupCurrencyListGrain()
        {
            List<Currency> lstCurrencies = new List<Currency>();
            try
            {
                lstCurrencies = await ReadFromJson.ReadDataFromJson<Currency>(Constants.CurrencyList);
                var grain = GrainClient.GrainFactory.GetGrain<ICurrency>(Constants.CurrencyList);
                await grain.SetItem(lstCurrencies);
            }
            catch (Exception) { }
        }
    }
}
