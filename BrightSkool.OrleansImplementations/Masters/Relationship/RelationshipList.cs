﻿using BrightSkool.GrainInterface.Masters;
using BrightSkool.OrleansImplementations.AppUtils;
using Orleans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.OrleansImplementations.Masters
{
    public class RelationshipList
    {
        public static async Task SetupRelationshipListGrain()
        {
            List<Relationship> lstRelationships = new List<Relationship>();
            try
            {
                lstRelationships = await ReadFromJson.ReadDataFromJson<Relationship>(Constants.RelationshipList);
                var grain = GrainClient.GrainFactory.GetGrain<IRelationship>(Constants.RelationshipList);
                await grain.SetItem(lstRelationships);
            }
            catch (Exception) { }
        }
    }
}
