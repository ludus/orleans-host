﻿using Orleans.Providers;
using System.Collections.Generic;
using Patterns.SmartCache.Implementation;
using BrightSkool.GrainInterface.Masters;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.OrleansImplementations.Masters
{
    [StorageProvider(ProviderName = "AzureStore")]
    public class NationalityGrain : CachedItemGrain<List<Nationality>>, INationality
    {
    }
}
