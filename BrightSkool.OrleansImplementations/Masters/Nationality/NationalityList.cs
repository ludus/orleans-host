﻿using BrightSkool.GrainInterface.Masters;
using BrightSkool.OrleansImplementations.AppUtils;
using Newtonsoft.Json;
using Orleans;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.OrleansImplementations.Masters
{
    public class NationalityList
    {
        public static async Task SetupNationalityListGrain()
        {
            List<Nationality> lstNationalities = new List<Nationality>();
            try
            {
                lstNationalities = await ReadFromJson.ReadDataFromJson<Nationality>(Constants.NationalityList);
                var grain = GrainClient.GrainFactory.GetGrain<INationality>(Constants.NationalityList);
                await grain.SetItem(lstNationalities);
            }
            catch (Exception) { }
        }
    }
}
