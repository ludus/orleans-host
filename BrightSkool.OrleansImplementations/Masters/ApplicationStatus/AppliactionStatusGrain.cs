﻿using Orleans.Providers;
using System.Collections.Generic;
using Patterns.SmartCache.Implementation;
using BrightSkool.GrainInterface.Masters;
using static BrightSkool.Domain.Models.CommonModel;
using BrightSkool.Domain.Models;

namespace BrightSkool.OrleansImplementations.Masters
{
    [StorageProvider(ProviderName = "AzureStore")]
    public class AppliactionStatusGrain : CachedItemGrain<List<ApplicationStatus>>, IApplicationStatus
    {
    }
}
