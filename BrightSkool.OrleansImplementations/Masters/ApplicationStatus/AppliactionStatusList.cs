﻿using BrightSkool.Domain.Models;
using BrightSkool.GrainInterface.Masters;
using BrightSkool.OrleansImplementations.AppUtils;
using Orleans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace BrightSkool.OrleansImplementations.Masters
{
    public class ApplicationStatusList
    {
        public static async Task SetupApplicationStatusListGrain()
        {
            List<ApplicationStatus> lstApplicationStatus = new List<ApplicationStatus>();
            try
            {
                lstApplicationStatus = await ReadFromJson.ReadDataFromJson<ApplicationStatus>(Constants.ApplicationStatusList);
                var grain = GrainClient.GrainFactory.GetGrain<IApplicationStatus>(Constants.ApplicationStatusList);
                await grain.SetItem(lstApplicationStatus);
            }
            catch (Exception) { }
        }
    }
}
