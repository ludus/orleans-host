﻿using BrightSkool.GrainInterface.Masters;
using BrightSkool.OrleansImplementations.AppUtils;
using Orleans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.OrleansImplementations.Masters
{
    public class LanguageList
    {
        public static async Task SetupLanguageListGrain()
        {
            List<Language> lstCountries = new List<Language>();
            try
            {
                lstCountries = await ReadFromJson.ReadDataFromJson<Language>(Constants.LanguageList);
                var grain = GrainClient.GrainFactory.GetGrain<ILanguage>(Constants.LanguageList);
                await grain.SetItem(lstCountries);
            }
            catch (Exception) { }
        }
    }
}
