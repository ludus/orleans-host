﻿using BrightSkool.GrainInterface.Masters;
using BrightSkool.OrleansImplementations.AppUtils;
using Orleans;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using static BrightSkool.Domain.Models.CommonModel;

namespace BrightSkool.OrleansImplementations.Masters
{
    public class RaceList
    {
        public static async Task SetupRaceListGrain()
        {
            List<Race> lstRace = new List<Race>();
            try
            {
                lstRace = await ReadFromJson.ReadDataFromJson<Race>(Constants.RaceList);
                var grain = GrainClient.GrainFactory.GetGrain<IRace>(Constants.RaceList);
                await grain.SetItem(lstRace);
            }
            catch (Exception) { }
        }
    }
}

//F:\Projects\Brightsword\Received\CICD Testing\BrightSkoolProject\BrightSkool.OrleansHost
