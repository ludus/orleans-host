﻿using System.Threading.Tasks;
using BrightSkool.Domain.Models;
using Orleans.Providers;
using Patterns.EventSourcing.Implementation;
using BrightSkool.GrainInterface.Registration;

namespace BrightSkool.OrleansImplementations.Registration
{
    [StorageProvider(ProviderName = "AzureStore")]
    public class RegistrationGrain :  EventSourcedGrain<RegistrationOperations, RegistrationState>, IRegistration
    {
        public Task<RegistrationModel> GetRegistration()
        {
            return Task.FromResult(State.CurrentState._registrationModel);
        }

        public Task SaveRegistration(RegistrationModel registrationmodel)
        {
            return ProcessEvent(RegistrationOperations.SaveRegistration(registrationmodel));
        }

        public Task ProcessRegistration(string status)
        {
            return ProcessEvent(RegistrationOperations.ProcessRegistration(status, State.CurrentState._registrationModel));
        }

        public void Dispose()
        {
            base.ClearStateAsync();
        }
    }
}
