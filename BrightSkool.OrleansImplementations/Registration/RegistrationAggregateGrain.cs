﻿using BrightSkool.GrainInterface.Registration;
using Patterns.Aggregates.Implementation;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BrightSkool.OrleansImplementations.Registration
{
    public class RegistrationAggregateGrain:
        LazilyComputedAggregateGrain
            <IRegistration, RegistrationOperations, RegistrationState, RegistrationAggregate>,
        IRegistrationAggregateGrain
    {
    }
}
