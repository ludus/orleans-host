﻿using Orleans.Providers;
using Patterns.SmartCache.Implementation;
using Patterns.Registry.Implementation;
using BrightSkool.GrainInterface.Registration;

namespace BrightSkool.OrleansImplementations.Registration
{
    public class RegistrationItemGrain : CachedItemGrain<RegistrationCatalogItem>, IRegistrationItemGrain
    {
    }

    [StorageProvider(ProviderName = "AzureStore")]
    public class RegistrationItemRegistryGrain : RegistryGrain<IRegistrationItemGrain>, IRegistrationRegistryGrain
    {
    }
}
