﻿using System;
using System.Collections.Generic;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using BrightSkool.GrainInterface.Registration;
using BrightSkool.Domain.Models;
using Orleans;

namespace TestingOrleansHost
{
    [TestClass]
    public class RegistrationGrainTest
    {
        /// <summary>
        /// this TestingSiloHost class is not working as expected
        /// </summary>
        //: TestingSiloHost

        //public RegistrationGrainTest()
        //    : base(new TestingSiloOptions
        //    {
        //        StartPrimary = true,
        //        StartSecondary = false,
        //        SiloConfigFile = new FileInfo("OrleansServerConfiguration.xml")
        //    })
        //{
        //}

        /// <summary>
        /// initialize the grainclient insde the constructor
        /// </summary>
        /// <returns></returns>
        public RegistrationGrainTest()
        {
            if (!GrainClient.IsInitialized)
            {
                try
                {
                    GrainClient.Initialize(System.IO.Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "OrleansClientConfiguration.xml"));
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Test case for retieving Registration object by passing ID
        /// </summary>
        /// <returns></returns>
        //[TestMethod]
        public async Task GetRegistrationTest()
        {
            //Arrange
            var RegistrationId = new Guid("80f81f8c-0d25-4c41-9bb6-1dfe316e2906");

            //Act
            IRegistration grain = GrainClient.GrainFactory.GetGrain<IRegistration>(RegistrationId);
            RegistrationModel _registrationmodel = await grain.GetRegistration();

            //Assert
            Assert.IsNotNull(_registrationmodel);
        }


        /// <summary>
        /// Test case for Get events for Registration domain
        /// </summary>
        /// <returns></returns>
        //[TestMethod]
        public async Task GetRegistrationEventTest()
        {
            //Arrange
            var SchoolId = new Guid("80f81f8c-0d25-4c41-9bb6-1dfe316e2906");
            IRegistration grain = GrainClient.GrainFactory.GetGrain<IRegistration>(SchoolId);

            //Act
            var events = await grain.GetEvents();

            //Assert
            Assert.IsNotNull(events);
            Assert.AreNotEqual(events.Count, 0);
        }


        /// <summary>
        /// Test case for saving Registration
        /// </summary>
        //[TestMethod]
        public void SaveRegistrationTest()
        {
            //Arrange
            RegistrationModel registration = new RegistrationModel();
            registration.Id = new Guid("80f81f8c-0d25-4c41-9bb6-1dfe316e2906");
            registration.PIN.FederalDocumentType = "Test document";
            registration.PIN.PersonalIDNumber = "Test number";
            registration.PIN.FederalDocumentType = "Test document";
            registration.Nationality.CountryNationality = "Indian";
            registration.Applicant.FirstName = "Test Applicant";
            registration.StudentAddress.AddressLine1 = "Test Address";
            registration.StudentAddress.City = "Test City";
            registration.StudentAddress.ZipCode = "560092";
            registration.StudentAddress.Country = "Test Country";
            registration.PhoneNumber.PhoneNumber = "Test Number";
            registration.Email = "test@mail.com";
            registration.Level = "Test Level";
            registration.SecondLanguage = "Test Language";

            //Act
            IRegistration grain = GrainClient.GrainFactory.GetGrain<IRegistration>(registration.Id);
            var task = grain.SaveRegistration(registration);
            while (!task.IsCompleted) ;

            //Assert
            Assert.IsTrue(task.IsCompleted);
        }
    }
}
