﻿using System;
using System.IO;
using System.Threading.Tasks;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Orleans;
using BrightSkool.OrleansImplementations.Masters;

namespace TestingOrleansHost
{
   // [TestClass]
    public class MastersGrainTest //: TestingSiloHost
    {
        /// <summary>
        /// initialize the grainclient insde the constructor
        /// </summary>
        /// <returns></returns>
        public MastersGrainTest()
        {
            if (!GrainClient.IsInitialized)
            {
                try
                {
                   
                    //GrainClient.Initialize(Path.Combine(AppDomain.CurrentDomain.BaseDirectory, "OrleansClientConfiguration.xml"));
                    GrainClient.Initialize(Path.Combine(Environment.GetEnvironmentVariable("RoleRoot")+ @"\", @"appRoot\" + "OrleansClientConfiguration.xml"));

                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
        }

        /// <summary>
        /// Test case for Create grain for countrylist
        /// </summary>
        /// <returns></returns>
        //[TestMethod]
        public async Task createCountrylistGrainTest()
        {
            //Arrange
            
            //Act
            var task= CountryList.SetupCountryListGrain();
            await task;

            //Assert
            Assert.IsTrue(task.IsCompleted);
        }

        /// <summary>
        /// Test case for Create grain for Currencylist
        /// </summary>
        /// <returns></returns>
        //[TestMethod]
        public async Task createCurrencyListGrainTest()
        {
            //Arrange

            //Act
            var task = CurrencyList.SetupCurrencyListGrain();
            await task;

            //Assert
            Assert.IsTrue(task.IsCompleted);
        }

        /// <summary>
        /// Test case for Create grain for Languagelist
        /// </summary>
        /// <returns></returns>
        //[TestMethod]
        public async Task createLanguageListGrainTest()
        {
            //Arrange

            //Act
            var task = LanguageList.SetupLanguageListGrain();
            await task;

            //Assert
            Assert.IsTrue(task.IsCompleted);
        }

        /// <summary>
        /// Test case for Create grain for Nationalitylist
        /// </summary>
        /// <returns></returns>
        //[TestMethod]
        public async Task createNationalityListGrainTest()
        {
            //Arrange

            //Act
            var task = NationalityList.SetupNationalityListGrain();
            await task;

            //Assert
            Assert.IsTrue(task.IsCompleted);
        }

        /// <summary>
        /// Test case for Create grain for Racelist
        /// </summary>
        /// <returns></returns>
        //[TestMethod]
        public async Task createRaceListGrainTest()
        {
            //Arrange

            //Act
            var task = RaceList.SetupRaceListGrain();
            await task;

            //Assert
            Assert.IsTrue(task.IsCompleted);
        }

        /// <summary>
        /// Test case for Create grain for Relationshiplist
        /// </summary>
        /// <returns></returns>
        //[TestMethod]
        public async Task createRelationshipListGrainTest()
        {
            //Arrange

            //Act
            var task = RelationshipList.SetupRelationshipListGrain();
            await task;

            //Assert
            Assert.IsTrue(task.IsCompleted);
        }

        /// <summary>
        /// Test case for Create grain for Relationshiplist
        /// </summary>
        /// <returns></returns>
        //[TestMethod]
        public async Task createReligionListGrainTest()
        {
            //Arrange

            //Act
            var task = ReligionList.SetupReligionListGrain();
            await task;

            //Assert
            Assert.IsTrue(task.IsCompleted);
        }
    }
}
