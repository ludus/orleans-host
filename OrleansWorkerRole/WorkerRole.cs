using System;
using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.ServiceRuntime;
using Orleans.Runtime.Host;
using Orleans.Runtime.Configuration;
using System.Linq;
using System.Collections.Generic;

namespace OrleansWorkerRole
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        public override void Run()
        {
            Trace.TraceInformation("OrleansWorkerRole is running");

            var config = AzureSilo.DefaultConfiguration();

            try
            {

                config.AddMemoryStorageProvider();
                ConfigureNewStorageProvider(config);
                silo = new AzureSilo();
                bool ok = silo.Start(config);
                silo.Run();
                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            catch (Exception ex)
            {
                Trace.Write(ex.InnerException);
                Trace.Write(RoleEnvironment.GetConfigurationSettingValue("DataConnectionString"));
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        private void ConfigureNewStorageProvider(ClusterConfiguration config)
        {
            const string myProviderFullTypeName = "Orleans.Storage.AzureTableStorage"; // Alternatively, can be something like typeof(AzureTableStorage).FullName
            const string myProviderName = "AzureStore"; // what ever arbitrary name you want to give to your provider

            var properties = new Dictionary<string, string>();
            string connectionString = RoleEnvironment.GetConfigurationSettingValue("DataConnectionString");
            properties.Add("DataConnectionString", connectionString);
            config.Globals.RegisterStorageProvider(myProviderFullTypeName, myProviderName, properties);
        }

        AzureSilo silo;
        public override bool OnStart()
        {
            #region Old Code
            //// Set the maximum number of concurrent connections
            //ServicePointManager.DefaultConnectionLimit = 12;

            //// For information on handling configuration changes
            //// see the MSDN topic at https://go.microsoft.com/fwlink/?LinkId=166357.

            //bool result = base.OnStart();

            //Trace.TraceInformation("OrleansWorkerRole has been started");

            //return result; 
            #endregion

            Trace.WriteLine("OrleansAzureSilos-OnStart called", "Information");
            Trace.WriteLine("OrleansAzureSilos-OnStart Initializing config", "Information");

            // Set the maximum number of concurrent connections 
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.
            RoleEnvironment.Changing += RoleEnvironmentChanging;
            SetupEnvironmentChangeHandlers();

            bool ok = base.OnStart();

            Trace.WriteLine("OrleansAzureSilos-OnStart called base.OnStart ok=" + ok, "Information");

            return ok;
        }

        private static void RoleEnvironmentChanging(object sender, RoleEnvironmentChangingEventArgs e)
        {
            int i = 1;
            foreach (var c in e.Changes)
            {
                Trace.WriteLine(string.Format("RoleEnvironmentChanging: #{0} Type={1} Change={2}", i++, c.GetType().FullName, c));
            }

            // If a configuration setting is changing);
            if (e.Changes.Any((RoleEnvironmentChange change) => change is RoleEnvironmentConfigurationSettingChange))
            {
                // Set e.Cancel to true to restart this role instance
                e.Cancel = true;
            }
        }

        private static void SetupEnvironmentChangeHandlers()
        {
            // For information on handling configuration changes see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.
        }

        public override void OnStop()
        {
            Trace.TraceInformation("OrleansWorkerRole is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("OrleansWorkerRole has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: Replace the following with your own logic.
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");
                await Task.Delay(1000);
            }
        }         
    }
}
